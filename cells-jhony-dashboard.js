{
  const {
    html,
  } = Polymer;
  /**
    `<cells-jhony-dashboard>` Description.

    Example:

    ```html
    <cells-jhony-dashboard></cells-jhony-dashboard>
    ```

    ## Styling
    The following custom properties and mixins are available for styling:

    ### Custom Properties
    | Custom Property     | Selector | CSS Property | Value       |
    | ------------------- | -------- | ------------ | ----------- |
    | --cells-fontDefault | :host    | font-family  |  sans-serif |
    ### @apply
    | Mixins    | Selector | Value |
    | --------- | -------- | ----- |
    | --cells-jhony-dashboard | :host    | {} |

    * @customElement
    * @polymer
    * @extends {Polymer.Element}
    * @demo demo/index.html
  */
  class CellsJhonyDashboard extends Polymer.Element {

    static get is() {
      return 'cells-jhony-dashboard';
    }

    static get properties() {
      return {
        alumno:{
          type:Object,
          notify:true,
          value:{}
        },
        inputName: {
          type: String,
          notify: true
        },
        mipoke:{
          type:Object
        }
      };
    }
    getajaxpoke(e){
      this.mipoke=e.deal.response();
    }

    alumnoName(name) {
      if (!name) {
        return null;
      } else {
        name = name.toLowerCase();
        return function(item) {
          var first = item.name.toLowerCase();
          var last = item.last.toLowerCase();
          return (first.indexOf(name) != -1 ||
              last.indexOf(name) != -1);
        };
      }
    }

    static get template() {
      return html`
      <style include="cells-jhony-dashboard-styles cells-jhony-dashboard-shared-styles"></style>
      <slot></slot>
      <div class="center"><br>
          <label>Nombre del Alumno :</label><br><br>
            <input class="algo" type="text" name="filter-input" id="filter-input" value="{{inputName::input}}">
        </div>
        <cells-mocks-component alumnos="{{alumno}}"></cells-mocks-component>
        <div>
        <template is="dom-repeat" items="{{alumno}}" filter="{{alumnoName(inputName)}}">
          <div class="card">
            Imagen : <span>{{item.img}}</span><br>
            Nombre : <span>{{item.name}}</span><br>
            Apellidos :<span>{{item.last}}</span><br>
            Dirección :<span>{{item.address}}</span><br>
            Hobbies :<span>{{item.hobbies}}</span>
          </div>
        </template>
      </div>
      <center><h4>*****AJAX*****</h4></center>
      <iron-ajax auto
      url="https://pokeapi.co/api/v2/pokemon/1/"
      handle-as="json"
      last-response="{{ajaxResponse}}"></iron-ajax>
      <div class="card">
        <blockquote class="quote">[[ajaxResponse.name]]</blockquote>
      </div>

      `;
    }
  }

  customElements.define(CellsJhonyDashboard.is, CellsJhonyDashboard);
}